//
//  NetworkManager.swift
//   TempoTask
//
//   Created by Apple on 6/25/21.
//


import RxSwift

class NetworkManager: NSObject, NetworkManagerProtocol {
    
    //MARK: - Constants
    private let baseUrl = "https://newsapi.org/v2/"
    var session: URLSession!
    static let shared = NetworkManager()
    
    //MARK: - CreateRequest
    /// Generic Function GreatRequest
    /// - Parameters:
    ///   - decodedType: way to make a type codable is to declare its properties
    ///   - urlPath: EndPoint Url
    ///   - queryParms: Query params allow for additional application state to be serialized into the URL
    ///   - method: Use this property to look up the method of the HTTP

    func createRequest<T:Codable>(decodedType: T.Type, urlPath: String,queryParms:[String:String], method: RequestMethod)->Observable<T> {
        return Observable.create({[weak self] observer in

            let urlString = "\(self?.baseUrl ?? "" )\(urlPath)"
            guard  var urlComponent = URLComponents(string: urlString) else{
                return Disposables.create()
            }
            urlComponent.queryItems = queryParms.map { (key, value) in
                    URLQueryItem(name: key, value: value)
                }
            urlComponent.queryItems?.append(.init(name: "apiKey", value: Constants.appKey.rawValue))
//            guard let url = URL(string: "\(self?.baseUrl ?? "" )\(urlPath)\(queryParams!)") else {return Disposables.create()}

            var urlRequest = URLRequest(url:  urlComponent.url!)
            
            urlRequest.httpMethod = method.rawValue
            urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
          //  urlRequest.httpBody = try! JSONSerialization.data(withJSONObject: params, options: [])
            
            let _ = self?.session.dataTask(with: urlRequest) { [weak self] data, response, error in
                do {
                    guard let _ = self else {return}
                    guard let data = data else {
                        if let error = error {
                            observer.onError(error)
                        }
                        return
                    }
                  //  let jsonSerilized = try JSONSerialization.
                    guard let jsonData = try? JSONDecoder().decode(decodedType, from: data)else {
                        return observer.onCompleted()
                    }
                    observer.onNext(jsonData)
                } catch let error {
                    observer.onError(error)
                }
            }.resume()
            return Disposables.create()
        })
    }
    
}

//MARK: - NetworkManagerProtocol
protocol NetworkManagerProtocol: AnyObject {
    func createRequest<T:Codable>(decodedType: T.Type,urlPath: String,queryParms:[String:String], method: RequestMethod)->Observable<T>
}
