//
//   NewsRepositry.swift
//   TempoTask
//
//   Created by Apple on 6/25/21.
//

import RxSwift

class NewsRepositry: NSObject, NewsRepositryProtocol {
    
    var session: URLSession = URLSession.shared
    
    //MARK: - Enums
    private enum EndPoint: String {
        case NewsList = "everything"
    }
    
    
    
    //MARK: - Get NewsDetails
    
    func getNewsList(queryText:String?,page:Int) throws-> Observable<News> {
        if page == 0 {
            throw NewListError.invalidPage("can't request with page number equal zero")
        }
        return Observable.create({[weak self] observer in
            NetworkManager.shared.session = self?.session
            let _ = NetworkManager.shared.createRequest(decodedType: News.self,urlPath: EndPoint.NewsList.rawValue,queryParms: ["q":queryText ?? "apple","page":"\(page)"], method: .GET).subscribe(onNext:{[weak self] newsList in
                guard let _ = self else {return}
                observer.onNext(newsList)
            }, onError: {[weak self] error in
                guard let _ = self else {return}
                observer.onError(error)
            }) {
                observer.onCompleted()
            }
            
            return Disposables.create()
        })
    }
}

//MARK: -  NewsRepositryProtocol
protocol NewsRepositryProtocol: AnyObject {
    func getNewsList(queryText:String?,page:Int)throws-> Observable<News>
    var session: URLSession{set get}
}
enum NewListError:Error{
    case invalidPage(String)
}
