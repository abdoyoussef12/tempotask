//
//  NewsDetailsViewController.swift
//  TempoTask
//
//  Created by Apple on 6/25/21.
//

import UIKit
import SDWebImage
import UIView_Shimmer
extension UILabel: ShimmeringViewProtocol { }
extension UIImageView: ShimmeringViewProtocol { }
extension UIButton: ShimmeringViewProtocol { }


class NewsDetailsViewController: UIViewController {
    var viewModel:NewsDetailsViewModel!
    @IBOutlet var newsImage: UIImageView!
    
    @IBOutlet var newsDateLabel: UILabel!
    @IBOutlet var newsLabel: UILabel!
    
    @IBOutlet var newsDesrptionLabel: UILabel!
    
    @IBOutlet var newsAuthorLabel: UILabel!
    
    @IBOutlet var newsSourceLabel: UILabel!
    
    @IBOutlet var newsContentLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        newsAuthorLabel.text =   viewModel.article.author
        newsDateLabel.text =   viewModel.article.publishedAt
        newsLabel.text =   viewModel.article.title
        newsDesrptionLabel.text =   viewModel.article.articleDescription
        newsSourceLabel.text =   viewModel.article.source.name
        newsContentLabel.text =   viewModel.article.content
        guard let url = URL(string: viewModel?.article.urlToImage ?? "") else{
            newsImage.image = #imageLiteral(resourceName: "default_image")
            return}
        
        newsImage.sd_setImage(with: url, completed: nil)

        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.view.setTemplateWithSubviews(false)
        }
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.setTemplateWithSubviews(true, viewBackgroundColor: .systemBackground)
    }
    @IBAction func goToSourcePressed(_ sender: Any) {
        let sourceViewModel = SourceViewModel(sourceTitle: viewModel.article.title, sourceLink: viewModel.article.url)
        let sourceVC = SourceRouter.createSourceViewConteroller(viewModel: sourceViewModel)
        self.navigationController?.pushViewController(sourceVC, animated: true)
        
    }
}
