//
//  NewsSourceViewController.swift
//  TempoTask
//
//  Created by Apple on 6/25/21.
//

import UIKit
import WebKit
class NewsSourceViewController: UIViewController {

    @IBOutlet var webKit: WKWebView!
    var viewModel:SourceViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = viewModel.sourceTitle
        setupWebView()
    }
    
    /// open Web view
    func setupWebView(){
        let url = URL(string: viewModel.sourceLink)!
        webKit.load(URLRequest(url: url))
        webKit.allowsBackForwardNavigationGestures = true
    }

}
