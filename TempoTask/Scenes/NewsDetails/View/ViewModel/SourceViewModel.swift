//
//  SourceViewModel.swift
//  TempoTask
//
//  Created by Apple on 6/25/21.
//

import Foundation
class SourceViewModel: BaseViewModel {
    
    // MARK: - Variables
    var sourceTitle : String!
    var sourceLink:String!
    // MARK: - Initialize
    init(sourceTitle:String,sourceLink:String) {
        super.init()
        self.sourceTitle = sourceTitle
        self.sourceLink = sourceLink
    }
}
