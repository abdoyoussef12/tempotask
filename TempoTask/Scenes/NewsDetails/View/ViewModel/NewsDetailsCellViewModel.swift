//
//  NewsDetailsCellViewModel.swift
//  TempoTask
//
//  Created by Apple on 6/25/21.
//

import Foundation
class NewsDetailsCellViewModel: BaseCellViewModel {
    
    // MARK: - Variables
    private let article: Article
    
    // MARK: - Initialize
    init(article: Article,cellIdentifier: String) {
        self.article = article
        super.init(cellIdentifier: cellIdentifier)
    }
    
    // MARK: - Helpers
    func getDetails()-> String {
        return article.articleDescription
    }
    
    func getsource() -> String {
        return article.source.name
    }
    
    func getImageLink()-> String{
        return article.urlToImage ?? ""
    }
    func getDate()-> String {
        return article.publishedAt ?? ""
    }
    func getTitle()-> String {
        return article.title
    }
    func getAuthor()-> String {
        return article.author ?? ""
    }
    func getContent()-> String {
        return article.content
    }
    func getUrl()-> String {
        return article.url
    }
}
