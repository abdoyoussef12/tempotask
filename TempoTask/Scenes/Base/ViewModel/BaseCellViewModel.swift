//
//  BaseCellViewModel.swift
//   TempoTask
//
//   Created by Apple on 6/25/21.
//

import UIKit

class BaseCellViewModel: NSObject {
    
    var cellIdentifier: String
    
    init(cellIdentifier: String) {
        self.cellIdentifier = cellIdentifier
        super.init()
    }
}
