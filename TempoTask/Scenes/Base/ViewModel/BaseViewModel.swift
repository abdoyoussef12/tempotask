//
//  BaseViewModel.swift
//   TempoTask
//
//   Created by Apple on 6/25/21.
//

import RxSwift

class BaseViewModel {
    
    var pushToView = PublishSubject<Void>()
}
