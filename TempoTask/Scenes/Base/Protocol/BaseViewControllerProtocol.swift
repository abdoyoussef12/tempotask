//
//  BaseViewControllerProtocol.swift
//   TempoTask
//
//   Created by Apple on 6/25/21.
//

import UIKit

protocol BaseViewControllerProtocol: AnyObject {
    init(viewModel: BaseViewModel)
}
