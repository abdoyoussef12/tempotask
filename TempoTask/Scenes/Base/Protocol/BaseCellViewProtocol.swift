//
//  BaseCellViewProtocol.swift
//   TempoTask
//
//   Created by Apple on 6/25/21.
//

import UIKit

protocol BaseCellViewProtocol: AnyObject {
    func configureCell(data: BaseCellViewModel)
}
 
extension UITableViewCell: BaseCellViewProtocol {
    @objc func configureCell(data: BaseCellViewModel) {
        
    }
    
    func asBaseCell()-> BaseCellViewProtocol {
        return self as BaseCellViewProtocol
    }
}
