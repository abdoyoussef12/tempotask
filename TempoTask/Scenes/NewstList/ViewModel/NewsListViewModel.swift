//
//   NewsListViewModel.swift
//   TempoTask
//
//   Created by Apple on 6/25/21.
//

import RxSwift
import RxCocoa
class NewsListViewModel: BaseViewModel {
    
    // MARK: - Variables
    private var cellViewModels:[NewsItemCellViewModel] = []
    
    var canShowTotalObserver = PublishSubject<Bool>()
    
    let dataSource = BehaviorSubject<[NewsItemCellViewModel]>(value: [])
    
    private let repositry: NewsRepositry!
    var searchText:String?
    var pageNumber = 1
    // create a subject and set the starter state, every time your viewModel
    // needs to show or hide a loading, just send an event
    let showLoading = BehaviorRelay<Bool>(value: false)
     var isLastPage = false
    // your async function
    private var disposeBag = DisposeBag()
    // MARK: - Initialize
    init(articles:[Article],repositry: NewsRepositry) {
        self.repositry = repositry
        super.init()
        self.initCellViewModels(article:articles)
    }
    
    // MARK: - InitCellViewModels
    private func initCellViewModels(article:[Article]) {
        let _ = article.map{self.cellViewModels.append(NewsItemCellViewModel(article: $0,cellIdentifier: "\(NewsListTableViewCell.self)"))}
    }
    
    // MARK: - ViewDidLoad
    func viewDidLoad() {
        canShowTotalObserver.onNext(false)
        fillTableDataSource()
    }
    
    func fillTableDataSource(){
        let _ = self.getNewsList(searchText: searchText).bind{[weak self] articles in
            articles.forEach{ article in
                self?.cellViewModels.append(.init(article: article, cellIdentifier: "\(NewsListTableViewCell.self)"))
            }
            self?.dataSource.onNext(self?.cellViewModels ?? [])
        }
    }
    
    
    // MARK: - GetNewsList
    /// func used to get list from Api
    /// - Parameter searchText: used to search
    
    private func getNewsList(searchText:String?)-> Observable<[Article]> {
        showLoading.accept(true)
        return Observable.create({[weak self] observer in
            let _ = try? self?.repositry.getNewsList(queryText: searchText,page: self!.pageNumber).subscribe(onNext: { (newsList) in
                self?.showLoading.accept(false)
                guard let _ = self else {return}
                observer.onNext(newsList.articles)
            }, onError: { (error) in
                
            }, onCompleted: {
                self?.isLastPage = true
                self?.showLoading.accept(false)
                observer.onCompleted()
            }).disposed(by:self!.disposeBag )
            return Disposables.create()
            
        }).observe(on: MainScheduler.asyncInstance)
    }
}

