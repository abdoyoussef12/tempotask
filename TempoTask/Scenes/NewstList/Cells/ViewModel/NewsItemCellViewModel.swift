//
//   NewsItemCellViewModel.swift
//   TempoTask
//
//   Created by Apple on 6/25/21.
//

import UIKit

class NewsItemCellViewModel: BaseCellViewModel {
    
    // MARK: - Variables
     let article: Article
    
    // MARK: - Initialize
    init(article: Article,cellIdentifier: String) {
        self.article = article
        super.init(cellIdentifier: cellIdentifier)
    }
    
    // MARK: - Helpers
    func getDetails()-> String {
        return article.articleDescription
    }
    
    func getsource() -> String {
        return article.source.name
    }
    
    func getImageLink()-> String{
        return article.urlToImage ?? ""
    }
  
}
