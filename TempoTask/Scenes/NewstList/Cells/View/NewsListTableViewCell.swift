//
//  NewsListTableViewCell.swift
//  TempoTask
//
//  Created by Apple on 6/25/21.
//

import UIKit
import SDWebImage


class NewsListTableViewCell: UITableViewCell {

    @IBOutlet var imageList: UIImageView!
    
    @IBOutlet var descriptionlable: UILabel!
    @IBOutlet var sourceLable: UILabel!
   
    override class func awakeFromNib() {
        
    }
    override func configureCell(data: BaseCellViewModel) {
        let viewModel = data as? NewsItemCellViewModel
        self.descriptionlable.text = viewModel?.getDetails()
        self.sourceLable.text = viewModel?.getsource()
        guard let url = URL(string: viewModel?.getImageLink() ?? "") else{
            imageList.image = #imageLiteral(resourceName: "default_image")
            return}
        
        imageList.sd_setImage(with: url, completed: nil)
        
    }
}
