//
//   NewsListViewController.swift
//   TempoTask
//
//   Created by Apple on 6/25/21.
//

import UIKit

import RxSwift

import RxCocoa
import UIView_Shimmer

class NewsListViewController: UIViewController, BaseViewControllerProtocol {
    
    // MARK: - IBOutlets
    @IBOutlet private weak var indicatorView: UIActivityIndicatorView!

    @IBOutlet private weak var tableView: UITableView!
    
    // MARK: - Variables
    private let viewModel: NewsListViewModel!
    private let disposeBag = DisposeBag()
    private let searchController = UISearchController()
//
    // MARK: - Initilaize
    required init(viewModel: BaseViewModel) {
        self.viewModel = viewModel as? NewsListViewModel
        super.init(nibName: "\(NewsListViewController.self)", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - InitViews
    private func initViews() {
        navigationItem.searchController = searchController
        configureTableView()
    }
    
    // MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        bindTableView()
        bindSearchBar()
        viewModel.viewDidLoad()
        viewModel.showLoading.asObservable().bind(to: indicatorView.rx.isAnimating).disposed(by: disposeBag)


    }
   
    // MARK: - ConfigureTableView
    private func configureTableView() {
        tableView.accessibilityIdentifier = "table--articleTableView"
        tableView.register(UINib(nibName: "\(NewsListTableViewCell.self)", bundle: nil), forCellReuseIdentifier: "\(NewsListTableViewCell.self)")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 500
    }

    // MARK: - BindTableView
    /// Fetch Data in TablelVIew
    private func bindTableView() {
        viewModel.dataSource.bind(to: tableView.rx.items(cellIdentifier: "\(NewsListTableViewCell.self)", cellType: NewsListTableViewCell.self)) {[weak self] (row,item,cell) in
            guard let _ = self else {return}
            cell.selectionStyle = .none
            cell.asBaseCell().configureCell(data: item)
        }.disposed(by: disposeBag)
        
        
        
        tableView.rx.modelSelected(NewsItemCellViewModel.self).subscribe(onNext: {[weak self] item in
         
            let controller = NewsDetailsRouter.createDetailsViewController(article: item.article)
                self!.navigationController!.pushViewController(controller, animated: true)
        }).disposed(by: disposeBag)
        
        
        bindPagination()
    }
    
    /// Used to reload Page size: 20
    fileprivate func bindPagination() {
        viewModel.dataSource.observe(on: MainScheduler.instance)
            .filter{
                !$0.isEmpty}
            .flatMap { _ in
                self.tableView.rx.contentOffset
            }
            .flatMap { (_) -> Observable<Bool> in
                let offsetY         = self.tableView.contentOffset.y
                let contentHeight   = self.tableView.contentSize.height
                let height          = self.tableView.frame.size.height
                
                if offsetY > contentHeight - height {
                    return   Observable.just(true)
                }
                return Observable.just(false)
            }.distinctUntilChanged()
            .filter{ _ in
                return !self.viewModel.isLastPage
            }
            .subscribe (onNext:{ (isReqeuest) in
                if isReqeuest{
                    self.viewModel.pageNumber+=1
                    
                    // just For Test
                   // DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        self.viewModel.fillTableDataSource()
                        
                   // }
                }
            }).disposed(by: disposeBag)
    }
 
    // MARK: - bindSearchBar
    private func bindSearchBar(){
        Observable.zip(
            searchController.searchBar.rx.text,
            searchController.searchBar.rx.searchButtonClicked)
            .map { _ in return self.searchController.searchBar.text ?? "" }
            .distinctUntilChanged()
            .subscribe(onNext: { (text) in
                let newsListVC = NewsListRouter.createListViewController(articles: [],searchText: text)
                self.navigationController?.pushViewController(newsListVC, animated:true)
            })
            .disposed(by: self.disposeBag)
        
    }
    
    
}

