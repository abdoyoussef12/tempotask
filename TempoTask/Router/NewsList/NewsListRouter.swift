//
//   NewsLstRouter.swift
//   TempoTask
//
//   Created by Apple on 6/25/21.
//

import UIKit

class NewsListRouter: NewsListRouterProtocol {
    
    //MARK: - CreateListViewController
    static func createListViewController(articles: [Article],searchText:String?)-> UIViewController {
        let repositry = NewsRepositry()
        let viewModel = NewsListViewModel(articles: articles,repositry: repositry)
        viewModel.searchText = searchText
        
        let viewController = NewsListViewController(viewModel: viewModel)
        return viewController
    }
}

//MARK: -  NewsListRouterProtocol
protocol NewsListRouterProtocol: AnyObject {
    static func createListViewController(articles: [Article],searchText:String?)-> UIViewController
}

