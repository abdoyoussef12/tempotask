//
//  SourceRouter.swift
//  TempoTask
//
//  Created by Apple on 6/25/21.
//

import UIKit
class SourceRouter{
    //MARK: - createSourceViewConteroller

    static func createSourceViewConteroller(viewModel:SourceViewModel)-> UIViewController{
        let viewController = NewsSourceViewController()
        viewController.viewModel = viewModel
        return viewController
    }
}
