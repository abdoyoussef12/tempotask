//
//   NewsDetailsRouter.swift
//   TempoTask
//
//  Created by Apple on 25/06/2021.
//

import UIKit

class NewsDetailsRouter: NewsDetailsRouterProtocol {
    
    //MARK: - CreateDetailsViewController
    static func createDetailsViewController(article: Article)-> UIViewController {
        let viewModel = NewsDetailsViewModel(article: article)

        let viewController = NewsDetailsViewController(nibName: "NewsDetailsViewController", bundle: nil)
        viewController.viewModel = viewModel
        return viewController
    }
}

//MARK: -  NewsDetailsRouterProtocol
protocol NewsDetailsRouterProtocol: AnyObject {
  static func createDetailsViewController(article: Article)-> UIViewController
}


