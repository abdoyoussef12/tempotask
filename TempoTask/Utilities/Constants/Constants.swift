//
//  Constants.swift
//   TempoTask
//
//   Created by Apple on 6/25/21.
//
import UIKit
// MARK: - Constants
enum Constants: String {
    case appKey = "1ba5b3521ed946c89e2a546e06ead784"
}

// MARK: - RequestMethod
enum RequestMethod: String {
    case GET
    case POST
}

