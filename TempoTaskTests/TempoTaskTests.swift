//
//  TempoTaskTests.swift
//  TempoTaskTests
//
//  Created by Apple on 6/24/21.
//

import XCTest
@testable import TempoTask

class TempoTaskTests: XCTestCase {
    var repo: NewsRepositry!
    
    override func setUpWithError() throws {
    repo = NewsRepositry()
    }

    override func tearDownWithError() throws {
        repo = nil
            super.tearDown()

    }
    
    func test_is_valid_request_without_query() throws {
        try!repo.getNewsList(queryText: nil, page: 1)
    }
    
    func test_is_valid_request_with_query() throws {
        try!repo.getNewsList(queryText: "apple", page: 1)
    }
    func test_invalid_page_reuest(){
       XCTAssertNoThrow(try repo.getNewsList(queryText: "apple", page: 0)) 
    }
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
